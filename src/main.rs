extern crate glib;
extern crate gstreamer as gst;

use crate::gst::prelude::*;

use anyhow::Result;

use chrono::prelude::*;

use std::collections::VecDeque;
use std::sync::{Arc, Mutex};

// PadWatchdog::stats() will return this struct
#[derive(Debug)]
pub struct PadWatchdogStats {
    // utc at the last buffer flow
    last_activity: DateTime<Utc>,

    // spent time since the last_activity when PadWatchdog::stats() is called
    since_last_activity: chrono::Duration,

    // calculated #buffer per second
    avg_num_buf_per_sec: f64,

    // calculated buffer duration per second, if there's gap between buffers
    // (i.e., timestap + duration != next timestamp), this would be smaller
    // than expected value.
    // If buffer duration is unknown, this value will be zero
    avg_dur_buf_per_sec: chrono::Duration,

    // the amount of running time increment per second within
    // given window size (i.e., Stats::stats_length)
    // this will not consider gaps between buffers.
    running_time_inc_per_sec: chrono::Duration,
}

#[derive(Debug)]
pub struct PadWatchdog {
    stats: Arc<Mutex<Stats>>,
    pad: Mutex<Option<(glib::WeakRef<gst::Pad>, gst::PadProbeId)>>,
}

impl PadWatchdog {
    pub fn new(stats_length: usize, pad: &gst::Pad) -> Self {
        let stats = Arc::new(Mutex::new(Stats {
            last_activities: VecDeque::with_capacity(stats_length),
            stats_length,
        }));

        let stats_clone = stats.clone();
        let probe_id = pad
            .add_probe(
                gst::PadProbeType::BUFFER | gst::PadProbeType::BUFFER_LIST,
                move |_, probe_info| {
                    let mut timestamp = gst::CLOCK_TIME_NONE;
                    let mut duration = gst::CLOCK_TIME_NONE;

                    if let Some(gst::PadProbeData::Buffer(ref buffer)) = probe_info.data {
                        timestamp = buffer.get_dts_or_pts();
                        duration = buffer.get_duration();
                    } else if let Some(gst::PadProbeData::BufferList(ref buffer_list)) =
                        probe_info.data
                    {
                        let (t, d) = Self::get_buffer_list_timestamp_and_duration(buffer_list);
                        timestamp = t;
                        duration = d;
                    }

                    let mut stats = stats_clone.lock().unwrap();
                    let stat_elem = StatElement {
                        utc: Utc::now(),
                        timestamp,
                        duration,
                    };

                    stats.last_activities.push_front(stat_elem);
                    let stats_length = stats.stats_length;
                    stats.last_activities.truncate(stats_length);

                    gst::PadProbeReturn::Pass
                },
            )
            .expect("Can't add pad probe");

        Self {
            stats,
            pad: Mutex::new(Some((pad.downgrade(), probe_id))),
        }
    }

    // Get timestamp and duration from GstBufferList.
    // timestamp: the timestamp of the first buffer
    // duration: timestamp difference between the newset/oldest buffers
    //           (+ duration of the newest buffer if any)
    fn get_buffer_list_timestamp_and_duration(
        buffer_list: &gst::BufferList,
    ) -> (gst::ClockTime, gst::ClockTime) {
        if buffer_list.len() > 0 {
            let buf = buffer_list.get(0).unwrap();
            let timestamp = buf.get_dts_or_pts();

            // just calculate time diff between the first and last
            let buf = buffer_list.get(buffer_list.len() as u32 - 1).unwrap();
            let mut last_timestamp = buf.get_dts_or_pts();
            if buf.get_duration() != gst::CLOCK_TIME_NONE {
                last_timestamp += buf.get_duration();
            }

            let duration = last_timestamp - timestamp;

            return (timestamp, duration);
        }

        (gst::CLOCK_TIME_NONE, gst::CLOCK_TIME_NONE)
    }

    // calculate average buffer duration per second
    // If buffer duration is not available (i.e., GST_CLOCK_TIME_NONE),
    // this method will just return zero
    fn calculate_avg_duration(
        stat_list: &VecDeque<StatElement>,
        utc_diff: i64,
    ) -> chrono::Duration {
        // something wrong...
        if utc_diff <= 0 {
            return chrono::Duration::zero();
        }

        let sum_buf_dur = stat_list
            .iter()
            .fold(gst::ClockTime::from(0), |acc, it| acc + it.duration);
        if sum_buf_dur == gst::CLOCK_TIME_NONE {
            // give up
            return chrono::Duration::zero();
        }

        let sum_buf_dur = sum_buf_dur.nanoseconds();
        if let Some(sum_buf_dur) = sum_buf_dur {
            let avg_dur = sum_buf_dur.mul_div_floor(gst::SECOND_VAL, utc_diff as u64);
            if let Some(avg_dur) = avg_dur {
                return chrono::Duration::nanoseconds(avg_dur as i64);
            }
        }

        chrono::Duration::zero()
    }

    fn calculate_running_time_increment(
        front: &StatElement,
        back: &StatElement,
        utc_diff: i64,
    ) -> chrono::Duration {
        // something wrong...
        if utc_diff <= 0 {
            return chrono::Duration::zero();
        }

        if front.timestamp == gst::CLOCK_TIME_NONE || back.timestamp == gst::CLOCK_TIME_NONE {
            return chrono::Duration::zero();
        }

        let mut inc = front.timestamp - back.timestamp;
        if front.duration != gst::CLOCK_TIME_NONE {
            inc += front.duration;
        }

        if let Some(inc) = inc.nanoseconds() {
            let avg_dur = inc.mul_div_floor(gst::SECOND_VAL, utc_diff as u64);
            if let Some(avg_dur) = avg_dur {
                return chrono::Duration::nanoseconds(avg_dur as i64);
            }
        }

        chrono::Duration::zero()
    }

    pub fn stats(&self) -> Option<PadWatchdogStats> {
        let stats = self.stats.lock().unwrap();
        if let Some(back) = stats.last_activities.back() {
            if let Some(front) = stats.last_activities.front() {
                let now = Utc::now();
                let diff = now - back.utc;
                if let Some(diff) = diff.num_nanoseconds() {
                    return Some(PadWatchdogStats {
                        last_activity: front.utc,
                        since_last_activity: now - front.utc,
                        avg_num_buf_per_sec: stats.last_activities.len() as f64 / diff as f64
                            * 1_000_000_000.0,
                        avg_dur_buf_per_sec: Self::calculate_avg_duration(
                            &stats.last_activities,
                            diff,
                        ),
                        running_time_inc_per_sec: Self::calculate_running_time_increment(
                            front, back, diff,
                        ),
                    });
                }
            }
        }
        None
    }
}

impl Drop for PadWatchdog {
    fn drop(&mut self) {
        let (pad, probe_id) = self.pad.lock().unwrap().take().expect("Double drop");
        if let Some(pad) = pad.upgrade() {
            pad.remove_probe(probe_id);
        }
    }
}

#[derive(Debug)]
struct StatElement {
    utc: DateTime<Utc>,
    timestamp: gst::ClockTime,
    duration: gst::ClockTime,
}

#[derive(Debug)]
struct Stats {
    last_activities: VecDeque<StatElement>,
    stats_length: usize,
}

fn main() -> Result<()> {
    gst::init()?;

    let pipeline = gst::parse_launch(
        "videotestsrc is-live=true ! valve name=v ! video/x-raw,framerate=30/1 ! autovideosink",
    )?
    .downcast::<gst::Pipeline>()
    .expect("Not a pipeline");

    let valve = pipeline.get_by_name("v").expect("No valve");
    let srcpad = valve.get_static_pad("src").expect("No srcpad on valve");

    let watchdog = PadWatchdog::new(30, &srcpad);
    glib::timeout_add_seconds(1, move || {
        println!("stats: {:?}", watchdog.stats());

        glib::Continue(true)
    });

    glib::timeout_add_seconds(5, move || {
        let drop = valve
            .get_property("drop")
            .unwrap()
            .get_some::<bool>()
            .unwrap();
        valve.set_property("drop", &(!drop)).unwrap();

        glib::Continue(true)
    });

    let loop_ = glib::MainLoop::new(None, false);

    pipeline.set_state(gst::State::Playing)?;

    let bus = pipeline.get_bus().expect("No bus");
    let loop_clone = loop_.clone();
    bus.add_watch(move |_bus, msg| {
        use gst::MessageView;

        match msg.view() {
            MessageView::Error(err) => {
                println!(
                    "Error from {:?}: {} ({:?})",
                    err.get_src().map(|s| s.get_path_string()),
                    err.get_error(),
                    err.get_debug()
                );
                loop_clone.quit();
            }
            MessageView::Warning(warn) => {
                println!(
                    "Warning from {:?}: {} ({:?})",
                    warn.get_src().map(|s| s.get_path_string()),
                    warn.get_error(),
                    warn.get_debug()
                );
            }
            MessageView::Eos(_) => {
                println!("eos");
                loop_clone.quit();
            }
            _ => (),
        }

        glib::Continue(true)
    })
    .expect("Can't add bus watch");

    loop_.run();

    pipeline.set_state(gst::State::Null)?;
    bus.remove_watch().expect("Can't remove bus watch");

    Ok(())
}
